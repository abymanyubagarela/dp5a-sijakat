<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		// if (empty($this->session->userdata['auth'])) {
		// 	$this->session->set_flashdata('failed', 'Anda Harus Login');

		// 	redirect('login');
		// } 

		$this->data = array(
             'controller'=>'user'
        );

		## load model here 
		$this->load->model('UserModel', 'User');
		$this->load->model('RoleModel', 'Role');
		$this->load->model('DivisiModel', 'Divisi');
		
	}

	public function index()	{	
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$data = $this->data;

		$data['title'] = 'User' ;
		$data['list'] = $this->User->getAllData();
		$data['role'] = $this->Role->getAllData();
		$data['divisi'] = $this->Divisi->getAllData();
		$data['column'] = $this->User->getColumn();	

		$this->load->view('inc/user', $data);
	}

	public function submitAddData() {
		$err = $this->User->inputData();
		
		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}
		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->User->getAllDataByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$err = $this->User->editData($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->User->deleteData($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function login()	{	

		$data = $this->data;
		
		if(!empty($_POST)) {
			
			$exist = $this->User->getLogin();
			
			if (empty($exist)) {
				$this->session->set_flashdata('failed', 'User Not Found');
				redirect($this->data['controller'].'/login');		
			} else {
				$this->session->set_userdata('auth', $exist);		
				$this->session->set_flashdata('success', 'Selamat Datang');

				redirect('welcome');		
			}

	    }

		$this->load->view('inc/login', $data);
	}

	public function logout()	{	

		$this->session->sess_destroy();

		redirect($this->data['controller'].'/login');	
	}
}
