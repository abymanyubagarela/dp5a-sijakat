<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();

		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->load->model('KronologiModel', 'Kronologi');
		$this->load->model('KlienModel', 'Klien');
	}

	public function index()	{	

		$data['kronologi'] = $this->Kronologi->countData();
		$data['baru'] = $this->Kronologi->countNewData();
		$data['approved'] = $this->Kronologi->countApprovedData();
		$data['klien'] = $this->Klien->countData();

		$this->load->view('inc/dashboard',$data);		
	}
}
