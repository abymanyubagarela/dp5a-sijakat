<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kronologi extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
             'controller'=>'kronologi'
        );

		## load model here 
		$this->load->model('KronologiModel', 'Kronologi');
		$this->load->model('KecamatanModel', 'Kecamatan');
		$this->load->model('KelurahanModel', 'Kelurahan');
		$this->load->model('AgamaModel', 'Agama');
		$this->load->model('JenispekerjaanModel', 'Kerja');
		$this->load->model('KlienModel', 'Klien');
		$this->load->model('DivisiModel', 'Divisi');
		$this->load->model('KategoriModel', 'Kategori');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Kronologi' ;
		$data['list'] = $this->Kronologi->getAllData();
		$data['column'] = $this->Kronologi->getColumn();
		$data['divisi'] = $this->Divisi->getAllData();
		$data['status'] = 0 ;

		if (!empty($_POST)) {
			$data['filter']['tgl_awal'] = $this->input->post('tgl_awal');
			$data['filter']['tgl_akhir'] = $this->input->post('tgl_akhir');
			$data['filter']['divisi'] = $this->input->post('divisi');
		}
		
		$this->load->view('inc/kronologi', $data);
	}

	public function inputData()	{	

		$data = $this->data;

		$data['title'] = 'Input Data Kronologi' ;
		
		$data['kecamatan'] = $this->Kecamatan->getAllData();
		$data['kelurahan'] = $this->Kelurahan->getAllData();
		$data['agama'] = $this->Agama->getAllData();
		$data['kerja'] = $this->Kerja->getAllData();
		$data['klien'] = $this->Klien->getAllData();
		$p = $this->Kategori->getListParent();
		$data['list'] = $p ;
		foreach ($p as $key => $value) {
			$a = $this->Kategori->getListChild($value->id);
			foreach ($a as $key => $value) {
				$data['list'][$key]['asu'] = 'sojfhoufjwr'; 
			}
		}
		
		print_r($data['list']);die();
die('xxxx');
		$this->load->view('inc/inputdatakronologi', $data);
	}

	public function editData($id)	{	

		$data = $this->data;

		$data['title'] = 'Edit Data Kronologi' ;	
		$data['edit'] = $this->Kronologi->getAllDataByID($id) ;	

		$data['kecamatan'] = $this->Kecamatan->getAllData();
		$data['kelurahan'] = $this->Kelurahan->getAllData();
		$data['agama'] = $this->Agama->getAllData();
		$data['kerja'] = $this->Kerja->getAllData();

		$this->load->view('inc/editdatakronologi', $data);
	}

	public function submitAddData() {

		$err = $this->Kronologi->inputData();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}
		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Kronologi->getAllDataByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {

		$err = $this->Kronologi->editData($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Kronologi->deleteData($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function prePrint($id) {	

		$data = $this->data;

		$res = $this->Kronologi->getReport($id) ;	
		
		$data['id_kronologi'] = $id;
		$data['pelapor'] = $res->nik.' - '.$res->name ;


		$this->load->view('inc/outreach', $data);
	}

	public function printData()	{	
		$data = $this->data;

		foreach ($_POST as $key => $row) {
            $data['pre'][$key] = $row;
        }		

        // print_r($data['pre']);die();
		$data['detail'] = $this->Kronologi->getReport($data['pre']['id_kronologi']) ;	
		
		$this->load->view('inc/outreachrepp', $data);
	}

	public function report()	{	

		$data = $this->data;

		$data['title'] = 'Cetak Data Kronologi' ;
		$data['list'] = $this->Kronologi->getAllData();
		$data['column'] = $this->Kronologi->getColumn();
		$data['status'] = 1 ;

		$this->load->view('inc/kronologi', $data);
	}
}
