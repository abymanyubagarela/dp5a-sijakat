<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
             'controller'=>'kategori'
        );

		## load model here 
		$this->load->model('KategoriModel', 'Kategori');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Kategori' ;
		$data['list'] = $this->Kategori->getAllData();
		$data['parent'] = $this->Kategori->getList();
		$data['column'] = $this->Kategori->getColumn();	
		

		$this->load->view('inc/kategori', $data);
	}

	public function submitAddData() {

		$err = $this->Kategori->inputData();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}
		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Kategori->getAllDataByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$err = $this->Kategori->editData($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Kategori->deleteData($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}
}
