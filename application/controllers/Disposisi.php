<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disposisi extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
             'controller'=>'disposisi'
        );

		## load model here 
		$this->load->model('KronologiModel', 'Kronologi');
		$this->load->model('KecamatanModel', 'Kecamatan');
		$this->load->model('KelurahanModel', 'Kelurahan');
		$this->load->model('AgamaModel', 'Agama');
		$this->load->model('JenispekerjaanModel', 'Kerja');
		$this->load->model('KlienModel', 'Klien');
		$this->load->model('DivisiModel', 'Divisi');
	}

	public function index()	{	
		// $x = strtotime('13-Feb-20');
		// // $x = strtotime('1/16/2020');

		// print_r(gettype($x));
		// $newformat = date('Y-m-d',$x);
		// print_r($newformat);die();
		$data = $this->data;

		$data['title'] = 'Disposisi' ;
		$data['column'] = $this->Kronologi->getColumn();
		$data['divisi'] = $this->Divisi->getAllData();
		$data['status'] = 0 ;

		$data['list'] = [];
		if (!empty($_POST)) {
			$data['filter']['tahun'] = $this->input->post('tahun');
			$data['filter']['bulan_awal'] = $this->input->post('bulan_awal');
			$data['filter']['bulan_akhir'] = $this->input->post('bulan_akhir');
			$data['filter']['divisi'] = $this->input->post('divisi');
			$data['list'] = $this->Kronologi->getAllDataDisposisi();
		}
		
		$this->load->view('inc/disposisi', $data);
	}


	public function report()	{	

		$data = $this->data;

		$data['title'] = 'Cetak Data Kronologi' ;
		$data['list'] = $this->Kronologi->getAllData();
		$data['column'] = $this->Kronologi->getColumn();
		$data['status'] = 1 ;

		$this->load->view('inc/disposisi', $data);
	}
}
