<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenispekerjaan extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
             'controller'=>'jenispekerjaan'
        );

		## load model here 
		$this->load->model('JenispekerjaanModel', 'Jenispekerjaan');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Jenis Pekerjaan' ;
		$data['list'] = $this->Jenispekerjaan->getAllData();
		$data['column'] = $this->Jenispekerjaan->getColumn();	

		$this->load->view('inc/jenispekerjaan', $data);
	}

	public function submitAddData() {

		$err = $this->Jenispekerjaan->inputData();

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
		}
		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Jenispekerjaan->getAllDataByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {
		$err = $this->Jenispekerjaan->editData($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Jenispekerjaan->deleteData($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}
}
