<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klien extends CI_Controller {

	var $data = array();
	function __construct() {
		parent::__construct();
		
		if (empty($this->session->userdata['auth'])) {
			$this->session->set_flashdata('failed', 'Anda Harus Login');

			redirect('login');
		} 

		$this->data = array(
             'controller'=>'klien'
        );

		## load model here 
		$this->load->model('KlienModel', 'Klien');
		$this->load->model('KecamatanModel', 'Kecamatan');
		$this->load->model('KelurahanModel', 'Kelurahan');
		$this->load->model('AgamaModel', 'Agama');
		$this->load->model('DivisiModel', 'Divisi');
		$this->load->model('JenispekerjaanModel', 'Kerja');
	}

	public function index()	{	

		$data = $this->data;

		$data['title'] = 'Klien' ;
		$data['list'] = $this->Klien->getAllData();
		$data['divisi'] = $this->Divisi->getAllData();
		$data['column'] = $this->Klien->getColumn();
		$data['status'] = 0 ;

		if (!empty($_POST)) {
			$data['filter']['tgl_awal'] = $this->input->post('tgl_awal');
			$data['filter']['tgl_akhir'] = $this->input->post('tgl_akhir');
			$data['filter']['divisi'] = $this->input->post('divisi');
		} 		

		$this->load->view('inc/klien', $data);
	}

	public function inputData()	{	

		$data = $this->data;

		$data['title'] = 'Input Data Klien' ;
		
		$data['kecamatan'] = $this->Kecamatan->getAllData();
		$data['kelurahan'] = $this->Kelurahan->getAllData();
		$data['agama'] = $this->Agama->getAllData();
		$data['kerja'] = $this->Kerja->getAllData();
		

		$this->load->view('inc/inputdataklien', $data);
	}

	public function editData($id)	{	

		$data = $this->data;

		$data['title'] = 'Edit Data Klien' ;	
		$data['edit'] = $this->Klien->getAllDataByID($id) ;	

		$data['kecamatan'] = $this->Kecamatan->getAllData();
		$data['kelurahan'] = $this->Kelurahan->getAllData();
		$data['agama'] = $this->Agama->getAllData();
		$data['kerja'] = $this->Kerja->getAllData();

		$this->load->view('inc/editdataklien', $data);
	}

	public function submitAddData() {
		$exist = $this->Klien->getAllDataByNIK($this->input->post('nik'));

		if (empty($exist)) {
			$err = $this->Klien->inputData();
			if ($err['code'] == '0') {
				$this->session->set_flashdata('success', 'Berhasil Menambahkan Data');
			} else {
				$this->session->set_flashdata('failed', 'Gagal Menambahkan Data');
			}
		} else {
			$this->session->set_flashdata('failed', 'Data dengan NIK tersebut sudah ada');
		}		
		
		redirect($this->data['controller']);
	}

	public function edit($id) {
		$data = $this->data;

		$data['list_edit'] = $this->Klien->getAllDataByID($id) ;

	    $this->output->set_content_type('application/json');
	    
	    $this->output->set_output(json_encode($data));

	    return $data;
	}

	public function submitEditData() {

		$err = $this->Klien->editData($this->input->post('id'));

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Merubah Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Merubah Data');
		}	

		redirect($this->data['controller']);
	}

	public function delete($id) {
		$err = $this->Klien->deleteData($id);

		if ($err['code'] == '0') {
			$this->session->set_flashdata('success', 'Berhasil Menghapus Data');
		} else {
			$this->session->set_flashdata('failed', 'Gagal Menghapus Data, Data Digunakan');
		}	

		redirect($this->data['controller']);
	}

	public function printData($id)	{	

		$data = $this->data;

		$data['title'] = 'Data Klien' ;	
		$data['edit'] = $this->Klien->getAllDataByID($id) ;	

		$data['kecamatan'] = $this->Kecamatan->getAllData();
		$data['kelurahan'] = $this->Kelurahan->getAllData();
		$data['agama'] = $this->Agama->getAllData();
		$data['kerja'] = $this->Kerja->getAllData();

		$this->load->view('inc/printdataklien', $data);
	}

	public function report() {
		$data = $this->data;

		$data['title'] = 'Cetak Data Klien' ;
		$data['list'] = $this->Klien->getAllData();
		$data['column'] = $this->Klien->getColumn();
		$data['status'] = 1 ;

		$this->load->view('inc/klien', $data);
	}
}
