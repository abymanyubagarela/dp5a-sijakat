<?php  

	class TransaksiKegiatanModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'tr_kegiatan' ;
	    }

	    function getAllData() {
	    	$this->db->select('tr_kegiatan.*, m_kegiatan.name as kegiatan , m_kegiatan.score as score, m_user.nama as pemilik, m_user.id_department,m_files.filename');
	        $this->db->join('m_kegiatan', 'm_kegiatan.id = tr_kegiatan.id_kegiatan', 'left');   
	        $this->db->join('m_user', 'm_user.id = tr_kegiatan.id_user', 'left');   
	        $this->db->join('m_files', 'm_files.id_tr_kegiatan = tr_kegiatan.id', 'left');   
	        $this->db->order_by("tr_kegiatan.id", "asc");

	    	$this->db->where(array('tr_kegiatan.is_active' => '1'));
	    	$this->db->group_by('tr_kegiatan.id');

	    	## filter
	    	if (!empty($_POST)) {
				if (!empty($_POST['t_awal'])) {
					$this->db->where('tr_kegiatan.date_created >=', $_POST['t_awal']);
				}

				if (!empty($_POST['t_akhir'])) {
					$this->db->where('tr_kegiatan.date_created <=', $_POST['t_akhir']);
				}

				if (!empty($_POST['status'])) {
					$this->db->where('tr_kegiatan.is_valid =', $_POST['status']);
				}

				
		    	
		    	

		    	if (empty($_POST['id_staff'])) {
		    		$this->db->where('m_user.id =', $this->session->userdata['auth']->id);
		    	} else {
		    		$this->db->where('m_user.id =', $_POST['id_staff']);
		    	}
			}
	    	## filter

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		function getAllDataByDiv($id) {

	    	$this->db->select('tr_kegiatan.*, m_kegiatan.name as kegiatan , m_kegiatan.score as score, m_user.nama as pemilik, m_user.id_department,m_files.filename');
	        $this->db->join('m_kegiatan', 'm_kegiatan.id = tr_kegiatan.id_kegiatan', 'left');   
	        $this->db->join('m_user', 'm_user.id = tr_kegiatan.id_user', 'left');   
	        $this->db->join('m_files', 'm_files.id_tr_kegiatan = tr_kegiatan.id', 'left');   
	    	
	    	$this->db->where(array('tr_kegiatan.is_active' => '1'));
	    	$this->db->where(array('tr_kegiatan.id_department' => $id));
	    	$this->db->group_by('tr_kegiatan.id'); 
	        
	    	## filter
	    	if (!empty($_POST)) {

				// $this->db->where('tr_kegiatan.date_created >=', $_POST['t_awal']);
				// $this->db->where('tr_kegiatan.date_created <=', $_POST['t_akhir']);
				// $this->db->where('tr_kegiatan.is_valid =', $_POST['status']);

				if (!empty($_POST['t_awal'])) {
					$this->db->where('tr_kegiatan.date_created >=', $_POST['t_awal']);
				}

				if (!empty($_POST['t_akhir'])) {
					$this->db->where('tr_kegiatan.date_created <=', $_POST['t_akhir']);
				}

				if (!empty($_POST['status'])) {
					$this->db->where('tr_kegiatan.is_valid =', $_POST['status']);
				}

		    	$this->db->where('tr_kegiatan.id_user =', $_POST['id_staff']);
			}
	    	## filter

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		function getAllDataByUser($id_user,$id_department) {

	    	$this->db->select('tr_kegiatan.*, m_kegiatan.name as kegiatan , m_kegiatan.score as score, m_user.nama as pemilik, m_user.id_department,m_files.filename');
	        $this->db->join('m_kegiatan', 'm_kegiatan.id = tr_kegiatan.id_kegiatan', 'left');   
	        $this->db->join('m_user', 'm_user.id = tr_kegiatan.id_user', 'left');   
	        $this->db->join('m_files', 'm_files.id_tr_kegiatan = tr_kegiatan.id', 'left');   
	    	
	    	$this->db->where(array('tr_kegiatan.is_active' => '1'));
	    	$this->db->where(array('tr_kegiatan.id_user' => $id_user));
	    	$this->db->group_by('tr_kegiatan.id');
	        
	    	## filter
	    	if (!empty($_POST)) {
				$this->db->where('tr_kegiatan.date_created >=', $_POST['t_awal']);
		    	$this->db->where('tr_kegiatan.date_created <=', $_POST['t_akhir']);
		    	$this->db->where('tr_kegiatan.is_valid =', $_POST['status']);
		    	
		    	if (empty($_POST['id_staff'])) {
		    		$this->db->where('tr_kegiatan.id_user =', $id_user);
		    	}
			}
	    	## filter

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		function getAllDataAdd() {
	    	$this->db->where(array('is_active' => '1'));
	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

	    function getAllDataByID($id) {
	        $data = array();

	        $this->db->where(array('id' => $id));	        
	        $query = $this->db->get($this->table_name);
	        $data['data'] = $query->row() ;

	        $this->db->where(array('id_tr_kegiatan' => $id));	        
	        $query = $this->db->get('m_files');
	        $data['images'] = $query->result() ;

	        return $data;
	    }

	    function getColumn() {
	        return $this->db->list_fields($this->table_name);
	    }

	    function inputData() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        if (empty($a_input['date_created'])) {
	        	$a_input['date_created'] = date('Y-m-d H:m:s');
	        }
	        
	        $a_input['is_active']	 = '1';
	        $a_input['is_valid']	 = '0';
	        
	        $a_input['id_user']	 = $this->session->userdata['auth']->id;
	        $a_input['id_department']	 = $this->session->userdata['auth']->id_department;
	        
	        $this->db->insert($this->table_name, $a_input);

	        $id = $this->db->insert_id();

	        $data = array('err'=> $this->db->error() , 'id' => $id );
	        return $data;	        
	     }

	    function editData($id) {
	        ## unset supaya id tidak terambil
	        unset($_POST['id']);

	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	      
	        
	        if ($this->session->userdata['auth']->id_role != 2) {
	        	unset($a_input);
	        	$a_input['validator'] =  $this->session->userdata['auth']->id;
	        	$a_input['is_valid'] = $_POST['is_valid'] ;

	        	if ($a_input['is_valid'] == 2 || $a_input['is_valid'] == 1) {
	        		$a_input['alasan'] = $_POST['alasan'] ;
	        	} else {
	        		$a_input['alasan'] = $_POST['alasan'] ;
	        	}
	        }

	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

		public function deleteData($id) {
			unset($_POST['id']);

			$this->db->select('id');
			$this->db->where(array('m_user.id_role' => $id)); 
			$role = $this->db->get('m_user')->row();

			if (empty($role)) {
				$a_input['is_active'] = '0';       
		        $this->db->where('id', $id);
		        $this->db->update($this->table_name, $a_input);

		        return $this->db->error();	      
			} else {
				return $err['code'] = 1 ;
			}       
		}

		public function inputDataFiles($file , $id , $column) {

	        $a_input = array();

	        $a_input['id_tr_kegiatan'] = $id;
	        $a_input['filename'] = $file;
	        $a_input['column']	 = $column;
	        
	        
	        $this->db->insert('m_files', $a_input);
	        
	        return $this->db->error();	  
	     }

	     public function inputDataFilesEdit($file , $id , $column) {

	        $a_input = array();

	        $a_input['id_tr_kegiatan'] = $id;
	        $a_input['filename'] = $file;
	        $a_input['column']	 = substr($column, 0,7);
	        
	        $this->db->delete('m_files', array('id_tr_kegiatan' => $a_input['id_tr_kegiatan'],'column' => $a_input['column']));

	        $this->db->insert('m_files', $a_input);
	        
	        return $this->db->error();	  
	     }

	     public function deleteimg($id,$col) {
			$this->db->delete('m_files', array('id_tr_kegiatan' => $id,'column' => substr($col, 0,7)));     
			return $this->db->error();	  
		}

		function editDataCheck($id) {
	        ## unset supaya id tidak terambil
	        unset($_POST['id']);

	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	      
	        
	        if ($this->session->userdata['auth']->id_role != 2) {
	        	$a_input['validator'] =  $this->session->userdata['auth']->id;

	        	if ($a_input['is_valid'] == 2) {
	        		$a_input['alasan'] = '-' ;
	        	}
	        }

	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

	    function editDataCheckAll() {
	        ## unset supaya id tidak terambil
	        $a_input = array();
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	      
	        $a_input['is_valid'] = 1;
	        
	        if ($this->session->userdata['auth']->id_department != 1) {
	        	$this->db->where('id_department', $this->session->userdata['auth']->id_department);
	        }
	        
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }
	}

?>