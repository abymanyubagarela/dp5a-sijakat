<?php  

	class KronologiModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_kronologi' ;
	    }

	    function getAllData() {
	    	$this->db->select('
	    		data_kronologi.date_created as tgl_buat,
	    		data_kronologi.*, 
	    		data_user.nama as created_name, 
	    		data_user.id as created_id, 
	    		data_klien.nik, 
	    		data_klien.no_kk, 
	    		data_klien.name, 
	    		data_klien.kelamin, 
	    		data_klien.pekerjaan, 
	    		data_klien.tgl_lahir, 
	    		data_klien.alamat,
	    		data_klien.alamat_kk,
	    		data_klien.telepon, 
	    	');

	    	$this->db->join('data_klien', 'data_klien.id = data_kronologi.id_klien', 'left');  
	    	$this->db->join('data_user', 'data_user.id = data_kronologi.created_name', 'left');  
	    	$this->db->where(array('data_kronologi.is_active' => '1'));
	    	
	    	
	    	if ($this->session->userdata['auth']->id_role == '3') {
	    		$this->db->where(array('data_kronologi.created_name' => $this->session->userdata['auth']->id));
	    	}

	    	if ($this->session->userdata['auth']->id_role == '2') {
	    		$this->db->where(array('data_user.id_divisi' => $this->session->userdata['auth']->id_divisi));
	    	} 

	    	if (!empty($_POST)) {
				$awal = date('Y-m-d',strtotime($this->input->post('tgl_awal')));
				$akhir = date('Y-m-d',strtotime($this->input->post('tgl_akhir')));

	    		$this->db->where('data_kronologi.date_created >=', $awal);
				$this->db->where('data_kronologi.date_created <=', $akhir);
				$this->db->where('data_user.id_divisi', $this->input->post('divisi'));
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

		function getAllDataDisposisi() {
	    	$this->db->select('
	    		data_kronologi.date_created as tgl_buat,
	    		data_kronologi.*, 
	    		data_klien.nik, 
	    		data_klien.no_kk, 
	    		data_klien.name, 
	    		data_klien.kelamin, 
	    		data_klien.pekerjaan, 
	    		data_klien.tgl_lahir, 
	    		data_klien.alamat,
	    		data_klien.alamat_kk,
	    		data_klien.telepon, 
	    		data_user.nama as created_name, 
	    		data_user.id as created_id'
	    	);

	    	$this->db->join('data_klien', 'data_klien.id = data_kronologi.id_klien', 'left');  
	    	$this->db->join('data_user', 'data_user.id = data_kronologi.created_name', 'left');  
	    	$this->db->where(array('data_kronologi.is_active' => '1'));
	    	
	    	
	    	if ($this->session->userdata['auth']->id_role == '3') {
	    		$this->db->where(array('data_kronologi.created_name' => $this->session->userdata['auth']->id));
	    	}

	    	if ($this->session->userdata['auth']->id_role == '2') {
	    		$this->db->where(array('data_user.id_divisi' => $this->session->userdata['auth']->id_divisi));
	    	} 

	    	if (!empty($_POST)) {
				$awal = date('Y-m-d',strtotime($this->input->post('bulan_awal'). '/01/'.$this->input->post('tahun')));
				$akhir =date('Y-m-d',strtotime($this->input->post('bulan_akhir'). '/31/'.$this->input->post('tahun')));

	    		$this->db->where('data_kronologi.date_created >=', $awal);
				$this->db->where('data_kronologi.date_created <=', $akhir);
				$this->db->where('data_user.id_divisi', $this->input->post('divisi'));
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

	    function getAllDataByID($id) {

	    	$this->db->select('data_kronologi.*, data_klien.nik , data_klien.name,m_kategori.name as kategori');

	    	$this->db->join('data_klien', 'data_klien.id = data_kronologi.id_klien', 'left');  
	    	$this->db->join('m_kategori', 'm_kategori.id = data_kronologi.id_kategori', 'left');  

	        $this->db->where(array('data_kronologi.id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    function getColumn() {
	        return $this->db->list_fields($this->table_name);
	    }

	    function inputData() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        foreach ($_FILES as $key => $value) {
	        	
				if (!empty($_FILES[$key]['name'])) {
					
					$config['upload_path']          = './uploads/kronologi/';
					$config['allowed_types']        = 'gif|jpg|png';
					$config['file_name']			= date('Ymd_Hms_').$key.'.'.pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);

					$a_input[$key] = $config['file_name'];

					$this->upload->initialize($config);	

					if ($this->upload->do_upload($key)) {
						if (!empty($this->upload->display_errors())) {
							return $this->db->error();	
				        } 
			        } 	
				}
				
			}

			$a_input['created_name'] = $this->session->userdata['auth']->id;
	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';

	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	     }

	    function editData($id) {
	    	## unset supaya id tidak terambil
	        unset($_POST['id']);

	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        foreach ($_FILES as $key => $value) {
	        	
				if (!empty($_FILES[$key]['name'])) {
					
					$config['upload_path']          = './uploads/kronologi/';
					$config['allowed_types']        = 'gif|jpg|png';
					$config['file_name']			= date('Ymd_Hms_').$key.'.'.pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);

					$a_input[$key] = $config['file_name'];

					$this->upload->initialize($config);	
					
					if ($this->upload->do_upload($key)) {
						if (!empty($this->upload->display_errors())) {
							return $this->db->error();	
				        } 
			        } 	
				}
				
			}

			if (!empty($a_input['kesimpulan'])) {
				$a_input['is_appoved'] = 1 ;
				$a_input['approved_by'] = $this->session->userdata['auth']->id ;				
			}

			$this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

		public function deleteData($id) {
			unset($_POST['id']);

			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
			/*
			$this->db->select('id');
			$this->db->where(array('m_user.id_department' => $id)); 
			$exist = $this->db->get('m_user')->row();

			if (empty($exist)) {
				$a_input['is_active'] = '0';       
		        $this->db->where('id', $id);
		        $this->db->update($this->table_name, $a_input);

		        return $this->db->error();	      
			} else {
				return $err['code'] = 1 ;
			}   
			*/   
		}

		function getReport($id) {
			$this->db->select('data_kronologi.*, data_klien.* , m_kecamatan.name as id_kecamatan, m_kelurahan.name as id_kelurahan, m_agama.name as id_agama, m_jeniskerja.name as id_jeniskerja, o.name as id_jeniskerja_o');
			$this->db->join('data_klien', 'data_klien.id = data_kronologi.id_klien', 'left');  
	    	
	    	$this->db->join('m_kecamatan', 'm_kecamatan.id = data_klien.id_kecamatan', 'left');
	    	$this->db->join('m_kelurahan', 'm_kelurahan.id = data_klien.id_kelurahan', 'left');
	    	$this->db->join('m_agama', 'm_agama.id = data_klien.id_agama', 'left');
	    	$this->db->join('m_jeniskerja', 'm_jeniskerja.id = data_klien.id_jeniskerja', 'left');
	    	$this->db->join('m_jeniskerja as o', 'o.id = data_klien.id_jeniskerja_o', 'left');


	        $this->db->where(array('data_kronologi.id' => $id));
	        
	        $query = $this->db->get($this->table_name);

	        return $query->row();
	    }

	    function countData(){
	    	return $this->db->count_all_results($this->table_name);
	    }

	    function countNewData(){
	    	$this->db->where('is_appoved', '0');
			return $this->db->count_all_results($this->table_name);
	    }

	    function countApprovedData(){
	    	$this->db->where('is_appoved', '1');
			return $this->db->count_all_results($this->table_name);
	    }
	}

?>