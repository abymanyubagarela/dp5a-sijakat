<?php  

	class KlienModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'data_klien' ;
	    }

	    function getAllData() {
	    	$this->db->select('data_klien.* , m_kecamatan.name as id_kecamatan , m_kelurahan.name as id_kelurahan, data_user.nama as created_name');
	    	$this->db->join('m_kecamatan', 'm_kecamatan.id = data_klien.id_kecamatan', 'left');
	    	$this->db->join('m_kelurahan', 'm_kelurahan.id = data_klien.id_kelurahan', 'left');
	    	$this->db->join('data_user', 'data_user.id = data_klien.created_name', 'left');  
	    	$this->db->where(array('data_klien.is_active' => '1'));

	    	if ($this->session->userdata['auth']->id_role == '3') {
	    		$this->db->where(array('data_klien.created_name' => $this->session->userdata['auth']->id));
	    	} 

	    	if ($this->session->userdata['auth']->id_role == '2') {
	    		$this->db->where(array('data_user.id_divisi' => $this->session->userdata['auth']->id_divisi));
	    	} 
	    	
	    	if (!empty($_POST)) {
	    		$this->db->where('data_klien.date_created >=', $this->input->post('tgl_awal'));
				$this->db->where('data_klien.date_created <=', $this->input->post('tgl_akhir'));
				$this->db->where('data_user.id_divisi', $this->input->post('divisi'));
	    	}

	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

	    function getAllDataByID($id) {
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    function getAllDataByNIK($id) {
	        $this->db->where(array('nik' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    function getColumn() {
	        return $this->db->list_fields($this->table_name);
	    }

	    function inputData() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['created_name'] = $this->session->userdata['auth']->id;
	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	     }

	    function editData($id) {
	        ## unset supaya id tidak terambil
	        unset($_POST['id']);

	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

		public function deleteData($id) {
			unset($_POST['id']);

			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
			/*
			$this->db->select('id');
			$this->db->where(array('m_user.id_department' => $id)); 
			$exist = $this->db->get('m_user')->row();

			if (empty($exist)) {
				$a_input['is_active'] = '0';       
		        $this->db->where('id', $id);
		        $this->db->update($this->table_name, $a_input);

		        return $this->db->error();	      
			} else {
				return $err['code'] = 1 ;
			}   
			*/   
		}

		function countData(){
	    	return $this->db->count_all_results($this->table_name);
	    }
	}

?>