<?php  

	class DivisiModel extends CI_Model
	{
		public function __construct() {
			parent::__construct();

	        ## declate table name here
	        $this->table_name = 'm_divisi' ;
	    }

	    function getAllData() {
	    	$this->db->where(array('is_active' => '1'));
	        $query = $this->db->get($this->table_name);

	        return $query->result();
		}

	    function getAllDataByID($id) {
	        $this->db->where(array('id' => $id));
	        
	        $query = $this->db->get($this->table_name);
	        
	        return $query->row();
	    }

	    function getColumn() {
	        return $this->db->list_fields($this->table_name);
	    }

	    function inputData() {
	        $a_input = array();
	       
	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }

	        $a_input['date_created'] = date('Y-m-d H:m:s');
	        $a_input['is_active']	 = '1';
	        
	        $this->db->insert($this->table_name, $a_input);

	        return $this->db->error();	        
	     }

	    function editData($id) {
	        ## unset supaya id tidak terambil
	        unset($_POST['id']);

	        foreach ($_POST as $key => $row) {
	            $a_input[$key] = $row;
	        }
	        $a_input['date_updated'] = date('Y-m-d H:m:s');	        

	        $this->db->where('id', $id);
	        $this->db->update($this->table_name, $a_input);

	        return $this->db->error();	        
	    }

		public function deleteData($id) {
			unset($_POST['id']);

			$a_input['is_active'] = '0';    
			
			$this->db->where('id', $id);

			$this->db->update($this->table_name, $a_input);

			return $this->db->error();	      
			/*
			$this->db->select('id');
			$this->db->where(array('m_user.id_department' => $id)); 
			$exist = $this->db->get('m_user')->row();

			if (empty($exist)) {
				$a_input['is_active'] = '0';       
		        $this->db->where('id', $id);
		        $this->db->update($this->table_name, $a_input);

		        return $this->db->error();	      
			} else {
				return $err['code'] = 1 ;
			}   
			*/   
		}
	}

?>