<script src="https://code.jquery.com/jquery.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<!-- swettalert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- swettalert -->



<script src="<?php echo base_url().'assets/front/'?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/plugins.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/main.js"></script>

<!--Chartist Chart-->
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/chartist/chartist.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/chartist/chartist-plugin-legend.js"></script> 

<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/datatables.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/jszip.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="<?php echo base_url().'assets/front/'?>assets/js/lib/data-table/datatables-init.js"></script>

<!-- select 2 -->
<script src="<?php echo base_url().'assets/boot/x/select2-4.0.4'?>/select2-4.0.4/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- select 2 -->

<script>
    jQuery(document).ready(function($) {
        "use strict"; 
        // Traffic Chart using chartist
        if ($('#traffic-chart').length) {
            var chart = new Chartist.Line('#traffic-chart', {
              labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
              series: [
              [0, 18000, 35000,  25000,  22000,  0],
              [0, 33000, 15000,  20000,  15000,  300],
              [0, 15000, 28000,  15000,  30000,  5000]
              ]
          }, {
              low: 0,
              showArea: true,
              showLine: false,
              showPoint: false,
              fullWidth: true,
              axisX: {
                showGrid: true
            }
        });

            chart.on('draw', function(data) {
                if(data.type === 'line' || data.type === 'area') {
                    data.element.animate({
                        d: {
                            begin: 2000 * data.index,
                            dur: 2000,
                            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                            to: data.path.clone().stringify(),
                            easing: Chartist.Svg.Easing.easeOutQuint
                        }
                    });
                }
            });
        }
        // Traffic Chart using chartist End


        
      <?php if (!empty($_SESSION['success'])) { ?>
        swal("Done", "<?php echo $_SESSION["success"] ?>", "success");
      <?php } ?>      

      <?php if (!empty($_SESSION['failed'])) { ?>
        swal("Done", "<?php echo $_SESSION["failed"] ?>", "error");
      <?php } ?>   


      $('#mastertable').dataTable( {
        "ordering": false
      });

      $(".e1").select2();   
      $(".select2-selection").css( "padding-top", "3px" );
      $(".select2-selection").css( "height", "35px" );
    
    });  // End of Document Ready
</script>