<!DOCTYPE html>
<html>

<head>
    <title>SiJakat</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <style type="text/css">
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: calc(100vh - 17px);
            font-family: 'Mukta', sans-serif;
            background-image: url(https://dp5a.surabaya.go.id/wp-content/uploads/2019/03/IMG_8022-1050x550.jpg);
            /*background-image: <?= base_url()."uploads/logo.jpg/"?>;*/
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        
        a {
            text-decoration: none;
            color: #444;
        }
        
        .title {
            text-align: center;
            margin-top: 15px;
            margin-bottom: 25px;
        }
        
        .subtitle {
            font-weight: 300;
            color: #444;
        }
        
        .brand {
            color: #1d5cf9;
            letter-spacing: 10px;
            font-size: 22px;
            font-weight: 700;
            padding-left: 8px;
        }
        
        .container {
            width: 320px;
            /*background-color: #ffffff9e;*/
            background-color: #ffffff;
            padding: 25px;
            border-radius: 12px;
            position: relative;
        }
        
        .container::after {
            content: '';
            width: 90%;
            left: 5%;
            bottom: 0px;
            height: 25px;
            z-index: -1;
            background-color: transparent;
            position: absolute;
            box-shadow: 0 10px 90px #add6f9;
        }
        
        form {
            margin-top: 60px;
        }
        
        .row {
            width: 100%;
        }
        
        .input-group {
            display: flex;
        }
        
        .input-group svg {
            font-size: 18px;
            border: 1px solid #eee;
            border-left: none;
            padding: 10px;
            color: #ddd;
            background-color: #fff;
        }
        
        .row input {
            width: 100%;
            font-size: 14px;
            border: 1px solid #eee;
            color: #000;
            height: 45px;
            padding-left: 10px;
        }
        
        .row input:focus {
            outline: none;
        }
        
        #username svg,
        #username input {
            border-bottom: none;
        }
        
        button {
            width: 50%;
            background-color: #1d5cf9;
            color: #fff;
            padding: 13px;
            font-size: 14px;
            border-radius: 100px;
            border: none;
            position: relative;
            left: 25%;
            box-shadow: 0 15px 40px #add6f9;
            margin: 35px 0;
            cursor: pointer;
            font-weight: 300;
        }
        
        button:focus {
            outline: none;
        }
        
        button svg {
            position: absolute;
            top: 25%;
            width: 15px;
            margin-left: 10px;
        }
        
        .forgotpassword {
            text-align: center;
            margin-top: 35px;
        }
        
        .singup {
            margin-top: 50px;
            text-align: center;
            color: #444;
            font-size: 14px;
        }
        
        .singup a {
            color: #1d5cf9;
            font-weight: 600;
        }

        span {
            font-size: 18px;
            border: 1px solid #eee;
            border-left: none;
            padding: 10px;
            color: #ddd;
            background-color: #fff;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="title">
            <div class="brand">
                <!-- OUTREACH -->
                <img src="<?=base_url('assets/front/images/logo.jpeg');?>" style="max-width: 70%" alt="Logo">
            </div>
        </div>

        <?php echo form_open_multipart($controller.'/login'); ?>
            <div class="row">
                <div class="input-group">
                    <input type="text" name="nik" class="" placeholder="NIK" required id="username">
                    <input id="go" placeholder="Go" readonly style="border-left: none;max-width: 10%;" onclick="show()">
                    <!-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user">
                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" onclick('alert()')></path>
                        <circle cx="12" cy="7" r="4"></circle>
                    </svg> -->
                </div>
            </div>

            <div class="row">
                <div class="input-group">                    
                    <input type="password" name="password" class="" placeholder="Password" id="password">
                </div>
            </div>
            

            <div class="row">
                <button type="Submit">LOGIN</button>
            </div>
        </form>    
    </div>

    <script src="https://code.jquery.com/jquery.js"></script>
    
    <script type="text/javascript">
        
        $(function() {
            $("#password").hide();
            $("#username").keyup(function(e) {

                if (e.keyCode === 8) {
                    $("#password").hide();
                } 
            });           

        });

        function show(){
            $("#password").show();
        }
    </script>
</body>

</html>