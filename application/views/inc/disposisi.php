<!doctype html>
<?php include 'header.php'; ?>

<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                                <!-- filter -->
                <?php if ($this->session->userdata['auth']->id_role == 1) { ?>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="panel-options">
                                <h5>Filter</h5>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <?php echo form_open_multipart($controller); ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="form-control-label">Tahun</label>
                                            <div class="form-group">
                                                <select class="form-control" name="tahun">
                                                <?php for($i=1;$i<=10;$i++) { ?>
                                                    <option value="<?= $i+2017 ?>" <?= !empty($filter) ? ($filter['tahun'] == $i+'2017' ? 'selected' : '') : '' ; ?>> <?= $i+2017 ?> </option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="form-control-label">Bulan Awal</label>
                                            <div class="form-group">
                                                <select class="form-control" name="bulan_awal">
                                                    <?php for($i=1;$i<=12;$i++) { ?>
                                                        <option value="<?= $i ?>" <?= !empty($filter) ? ($filter['bulan_awal'] == $i ? 'selected' : '') : '' ; ?> > <?= date('F',strtotime('2018-'.$i.'-01')) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label class="form-control-label">Bulan Akhir</label>
                                            <div class="form-group">
                                                <select class="form-control" name="bulan_akhir">
                                                    <?php for($j=1;$j<=12;$j++) { ?>
                                                        <option value="<?= $j ?>" <?= !empty($filter) ? ($filter['bulan_akhir'] == $j ? 'selected' : '') : '' ; ?> > <?= date('F',strtotime('2018-'.$j.'-01')) ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-control-label">Divisi</label>
                                            <select class="form-control" name="divisi">
                                                <?php foreach ($divisi as $key => $a) { ?>
                                                    <option value="<?php echo $a->id ?>" <?= isset($filter) ? ($filter['divisi'] == $key ? 'selected' : '')  : '' ?> ><?php echo $a->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>



                                        <div class="col-md-3">
                                            <label class="form-control-label">&nbsp;</label><br>
                                            <button type="submit" class="btn btn-sm btn-warning fa fa-search"></button>
                                        </div>
                                        
                                    </div>
                                    <span style="color: red">* Lakukan filter untuk menampilkan data.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- filter -->
                
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="panel-options">
                                <h5>Data Tabel Disposisi</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered mastertable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>NIK</th>
                                        <th>No KK</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Pekerjaan</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Alamat KK</th>
                                        <th>Alamat Domisili</th>
                                        <th>Telepon</th>
                                        <th>Bantuan</th>
                                        <th>Harapan</th>
                                        <th>Kronologi</th>
                                        <th>Tindak Lanjut</th>

                                        <?php if ($this->session->userdata['auth']->id_role != 3) { ?>
                                        <th>Petugas</th>
                                        <?php } ?>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0; ?>
                                    <?php foreach ($list as $key =>$row) { ?>
                                        <!-- <?php if ($i % 3 == 0): ?>
                                            <tr>
                                                <td colspan="16" style="background: yellow;color: white;">tes</td>
                                                <?php for ($i=0; $i < 15; $i++) { ?>
                                                    <td style="display: none;"></td>
                                                <?php } ?>
                                            </tr>
                                        <?php endif ?> -->
                                        <tr>                                                
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo date_format(date_create($row->tgl_buat),'d M Y'); ?></td>
                                            <td><?php echo $row->nik; ?></td>
                                            <td><?php echo $row->no_kk; ?></td>
                                            <td><?php echo $row->name; ?></td>
                                            <td><?php echo $row->kelamin === '1' ? 'P' : 'L'; ?></td>
                                            <td><?php echo $row->pekerjaan; ?></td>
                                            <td><?php echo date_format(date_create($row->tgl_lahir),'d-m-Y'); ?></td>
                                            <td><?php echo $row->alamat_kk; ?></td>
                                            <td><?php echo $row->alamat; ?></td>
                                            <td><?php echo $row->telepon; ?></td>
                                            <td><?php echo $row->bantuan; ?></td>
                                            <td><?php echo $row->harapan; ?></td>
                                            <td><?php echo $row->kronologi; ?></td>
                                            <td><?php echo $row->pasca; ?></td>
                                                
                                            <?php if ($this->session->userdata['auth']->id_role != 3) { ?>
                                                <td><?php echo !empty($row->created_name) ? ucwords($row->created_name) : ''; ?></td>
                                            <?php } ?>                   
                                        </tr>
                                    <?php } ?>                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->
    <?php include 'footer.php'; ?>

    <script type="text/javascript">
        function editdata(id) {
            url = "<?php echo site_url($controller.'/editdata'); ?>/"+id,
            $(location).attr('href', url);
        }

        function deletedata(id){
            var url="<?php echo site_url();?>";
            var r=confirm("Apakah Data ini ingin di Hapus?")
            if (r==true)
              window.location = url+"/<?php echo $controller; ?>/delete/"+id;
            else
              return false;
        }

        function printdata(id) {
            url = "<?php echo site_url($controller.'/preprint'); ?>/"+id,
            $(location).attr('href', url);
        }

        $('.mastertable').dataTable( {
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                'excel',
                {
                    extend: 'print',
                    text: 'PDF',
                    customize: function(win)
                        {
             
                            var last = null;
                            var current = null;
                            var bod = [];
             
                            var css = '@page { size: landscape; }',
                                head = win.document.head || win.document.getElementsByTagName('head')[0],
                                style = win.document.createElement('style');
             
                            style.type = 'text/css';
                            style.media = 'print';
             
                            if (style.styleSheet)
                            {
                              style.styleSheet.cssText = css;
                            }
                            else
                            {
                              style.appendChild(win.document.createTextNode(css));
                            }
             
                            head.appendChild(style);
                     }
                }
            ]
        });
    </script>

</body>
</html>
