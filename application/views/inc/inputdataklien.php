<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open_multipart($controller.'/submitadddata'); ?>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong><?php echo $title; ?></strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">NIK</label>
                                        <input type="number" class="form-control" name="nik">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="tgl_lahir">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Jenis Kelamin</label>
                                                 <select class="form-control" name="kelamin">
                                                    <option value="0">Laki - laki</option>
                                                    <option value="1">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Telepon</label>
                                                <input type="text" class="form-control" name="telepon">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Alamat Domisili</label>
                                        <textarea rows="2" class="form-control" name="alamat"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Alamat KK</label>
                                        <textarea rows="2" class="form-control" name="alamat_kk"></textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="form-control-label">No KK</label>
                                        <input type="number" class="form-control" name="no_kk">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kecamatan</label>
                                                 <select class="form-control" name="id_kecamatan">
                                                    <?php foreach ($kecamatan as $key => $c) { ?>
                                                        <option value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kelurahan</label>
                                                 <select class="form-control" name="id_kelurahan">
                                                    <?php foreach ($kelurahan as $key => $l) { ?>
                                                        <option value="<?php echo $l->id ?>"><?php echo $l->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Agama</label>
                                                 <select class="form-control" name="id_agama">
                                                    <?php foreach ($agama as $key => $a) { ?>
                                                        <option value="<?php echo $a->id ?>"><?php echo $a->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Status Perkawinan</label>
                                                 <select class="form-control" name="status">
                                                    <option value="0">Belum</option>
                                                    <option value="1">Kawin</option>
                                                    <option value="2">Bercerai</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Jenis Pekerjaan</label>
                                        <select class="form-control" name="id_jeniskerja">
                                            <?php foreach ($kerja as $key => $r) { ?>
                                                <option value="<?php echo $r->id ?>"><?php echo $r->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pekerjaan</label>
                                        <input type="text" class="form-control" name="pekerjaan">
                                    </div>

                                </div>
                            </div>
                        </div>

                         <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong>Data Pelaku</strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">NIK</label>
                                        <input type="number" class="form-control" name="nik">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" name="name">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="tgl_lahir">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Kategori</label>
                                                 <select class="form-control" name="status">
                                                    <option value="0">Anak-Anak</option>
                                                    <option value="1">Dewasa</option>
                                                    <option value="2">Lain-Lain</option>
                                                </select>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Jenis Kelamin</label>
                                                 <select class="form-control" name="kelamin">
                                                    <option value="0">Laki - laki</option>
                                                    <option value="1">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Telepon</label>
                                                <input type="text" class="form-control" name="telepon">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Pendidikan Terakhir</label>
                                                 <select class="form-control" name="id_kecamatan">
                                                    <?php foreach ($kecamatan as $key => $c) { ?>
                                                        <option value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Nama Sekolah/Perguruan Tinggi/Instansi</label>
                                                 <select class="form-control" name="id_kelurahan">
                                                    <?php foreach ($kelurahan as $key => $l) { ?>
                                                        <option value="<?php echo $l->id ?>"><?php echo $l->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Alamat Domisili</label>
                                        <textarea rows="2" class="form-control" name="alamat"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Alamat KK</label>
                                        <textarea rows="2" class="form-control" name="alamat_kk"></textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="form-control-label">No KK</label>
                                        <input type="number" class="form-control" name="no_kk">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kecamatan</label>
                                                 <select class="form-control" name="id_kecamatan">
                                                    <?php foreach ($kecamatan as $key => $c) { ?>
                                                        <option value="<?php echo $c->id ?>"><?php echo $c->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kelurahan</label>
                                                 <select class="form-control" name="id_kelurahan">
                                                    <?php foreach ($kelurahan as $key => $l) { ?>
                                                        <option value="<?php echo $l->id ?>"><?php echo $l->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Agama</label>
                                                 <select class="form-control" name="id_agama">
                                                    <?php foreach ($agama as $key => $a) { ?>
                                                        <option value="<?php echo $a->id ?>"><?php echo $a->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Status Perkawinan</label>
                                                 <select class="form-control" name="status">
                                                    <option value="0">Belum</option>
                                                    <option value="1">Kawin</option>
                                                    <option value="2">Bercerai</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Jenis Pekerjaan</label>
                                        <select class="form-control" name="id_jeniskerja">
                                            <?php foreach ($kerja as $key => $r) { ?>
                                                <option value="<?php echo $r->id ?>"><?php echo $r->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pekerjaan</label>
                                        <input type="text" class="form-control" name="pekerjaan">
                                    </div>

                                </div>
                            </div>
                        </div>
                   
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong>Data Wali/Orang Tua</strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" name="name_o">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Telepon</label>
                                        <input type="text" class="form-control" name="telepon_o">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Jenis Pekerjaan</label>
                                        <select class="form-control" name="id_jeniskerja_o">
                                            <?php foreach ($kerja as $key => $r) { ?>
                                                <option value="<?php echo $r->id ?>"><?php echo $r->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pekerjaan</label>
                                        <input type="text" class="form-control" name="pekerjaan_o">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Jml Saudara</label>
                                        <input type="text" class="form-control" name="jml_saudara">
                                    </div>

                                    <div class="form-group">
                                        <div class="row" style="padding: 0 30%">
                                            <div class="col-md-6"><button type="submit" class="btn btn-lg btn-info btn-block">Simpan</button></div>
                                            <div class="col-md-6">
                                                <a href="<?php echo base_url().'klien/inputdata'; ?>" class="btn btn-lg btn-danger btn-block">Batal</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2020 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://dp5a.surabaya.go.id">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  
</body>
</html>
