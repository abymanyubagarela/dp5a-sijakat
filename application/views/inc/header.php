<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SiJakat</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://d1qb2nb5cznatu.cloudfront.net/startups/i/535049-119614e6b2cc06a8a97c4e5b84570d5c-medium_jpg.jpg?buster=1508201111">
    <link rel="shortcut icon" href="https://d1qb2nb5cznatu.cloudfront.net/startups/i/535049-119614e6b2cc06a8a97c4e5b84570d5c-medium_jpg.jpg?buster=1508201111">

    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/pe-icon-7-filled.css">


    <link href="<?php echo base_url().'assets/front/'?>assets/weather/css/weather-icons.css" rel="stylesheet" />
    <link href="<?php echo base_url().'assets/front/'?>assets/calendar/fullcalendar.css" rel="stylesheet" />

    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/style.css">
    <link href="<?php echo base_url().'assets/front/'?>assets/css/charts/chartist.min.css" rel="stylesheet"> 
    <link href="<?php echo base_url().'assets/front/'?>assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet"> 
    <link rel="stylesheet" href="<?php echo base_url().'assets/front/'?>assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    

    <!-- swettalert -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!-- swettalert -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- select 2 -->
    <link href="<?php echo base_url().'assets/boot/x/select2-4.0.4'?>/select2-4.0.4/dist/css/select2.min.css" rel="stylesheet">
    <!-- select 2 -->

    <!-- CKEDITOR -->
    <script src="<?php echo base_url().'assets/front/'?>ckeditor/ckeditor.js"></script>
    <!-- CKEDITOR -->

    <style>
        #weatherWidget .currentDesc {
            color: #ffffff!important;
        }
        .traffic-chart { 
            min-height: 335px; 
        }
        #flotPie1  {
            height: 150px;
        } 
        #flotPie1 td {
            padding:3px;
        }
        #flotPie1 table {
            top: 20px!important;
            right: -10px!important;
        }
        .chart-container {
            display: table;
            min-width: 270px ;
            text-align: left;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        #flotLine5  {
             height: 105px;
        } 

        #flotBarChart {
            height: 150px;
        }
        #cellPaiChart{
            height: 160px;
        }

        @media print
        {    
            .no-print, .no-print *
            {
                display: none !important;
            }

            .right-panel {
                margin-left: 0px;
            }

            .photo {
                max-width: 50%;
            }

            .kop {
                max-width: 10%!important;
            }
        }

    </style>

    <!-- laporan outreach -->
    <style type="text/css">
        .dot {
            width: 10%;
            text-align: center;
        }

        .justify {
            text-align: justify;
        }

        .margin {
            margin: 2% 0;
        }

        .content-out {
            margin: 1% 0;
        }

        .title {
            font-weight: bold;
        } 

        .left {
            padding-left: 2%;
        }

        .left-out {
            padding-left: 10%;
        }

        .tembusan {
            padding-left: 10%;
        }

        .name {
            margin-top: 10%;
        }

        .center-out {
            text-align: center;
        }
    </style>
    <!-- laporan outreach -->

</head>