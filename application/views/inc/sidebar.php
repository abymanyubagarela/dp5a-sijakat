<aside id="left-panel" class="left-panel no-print">
    <nav class="navbar navbar-expand-sm navbar-default" style="background-color: #fff!important;border:none;"> 
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?php echo base_url().'welcome'; ?>"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="menu-title">E-Outreach</li><!-- /.menu-title -->

                
                <?php if ($this->session->userdata['auth']->id_role == 1){ ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bookmark"></i>Master Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'dinas'; ?>">Dinas</a></li>                            
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien'; ?>">Data Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi'; ?>">Data Kronologi</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kasus'; ?>">Jenis Kasus</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'jenispekerjaan'; ?>">Jenis Pekerjaan</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kecamatan'; ?>">Kecamatan</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kelurahan'; ?>">Desa/Kelurahan</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'agama'; ?>">Agama</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kategori'; ?>">Kategori</a></li>
                        </ul>
                    </li>   

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-cogs"></i>Konfigurasi</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'role'; ?>">Role</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'user'; ?>">User</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'divisi'; ?>">Divisi</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Input</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien/inputdata'; ?>">Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi/inputdata'; ?>">Kronologi</a></li>

                            <!-- <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi/inputdata'; ?>">Disposisi (Excel)</a></li> -->
                        </ul>
                    </li>   

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart-o"></i>Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien'; ?>">Data Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi'; ?>">Data Kronologi</a></li>
                            <!-- <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi'; ?>">Data Disposisi</a></li> -->
                        </ul>
                    </li>   

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Laporan</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi/report'; ?>">Cetak Kronologi</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien/report'; ?>">Cetak Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi'; ?>">Cetak Disposisi</a></li>
                        </ul>
                    </li> 
                <?php } else { ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Input</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien/inputdata'; ?>">Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi/inputdata'; ?>">Kronologi</a></li>
                            <!-- <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi/inputdata'; ?>">Disposisi (Excel)</a></li> -->
                        </ul>
                    </li>   

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart-o"></i>Data</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien'; ?>">Data Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi'; ?>">Data Kronologi</a></li>
                            <!-- <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi'; ?>">Data Disposisi</a></li> -->
                        </ul>
                    </li>   

                    <?php if ($this->session->userdata['auth']->id_role != 3) { ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book"></i>Laporan</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'klien/report'; ?>">Cetak Kronologi</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'kronologi/report'; ?>">Cetak Klien</a></li>
                            <li><i class="menu-icon ti-angle-right"></i><a href="<?php echo base_url().'disposisi'; ?>">Cetak Disposisi</a></li>
                        </ul>
                    </li> 
                    <?php } ?>
                <?php } ?>

                <li>
                    <a href="#"><i class="menu-icon fa fa-user"></i>  
                        <b> <?= ucwords($this->session->userdata['auth']->nama).' - ';?> <u><?= $this->session->userdata['auth']->divisi ?></u> </b>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url().'user/logout'; ?>"><i class="menu-icon fa fa-sign-out"></i>Logout </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>