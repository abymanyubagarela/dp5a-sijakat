<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="panel-options">
                                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalcreate"><i class="fa fa-plus"></i>&nbsp; Tambah <?php echo $title; ?></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="mastertable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Nama</th>
                                        <th>Keterangan</th>
                                        <th>Jumlah</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0; ?>
                                    <?php foreach ($list as $key =>$row) { ?>
                                        <tr>                                                
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo $row->name; ?></td>
                                            <td><?php echo $row->keterangan; ?></td>
                                            <td><?php echo $row->count; ?></td>

                                            <td width="15%" align="center">
                                                <button type="button" class="btn btn-sm btn-info" onclick="editdata(<?php echo $row->id;?>);">Edit</button>
                                                <button type="button" class="btn btn-sm btn-danger" onclick="deletedata(<?php echo $row->id;?>);">Delete</button>
                                            </td>                     
                                        </tr>
                                    <?php } ?>                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modalcreate" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <br><br>
                <div class="modal-body">                    
                    <?php echo form_open_multipart($controller.'/submitadddata'); ?>
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>Kasus</label>
                                    <input class="form-control" type="text" required name="name">
                                </div>     
                                <div class="col-md-12">
                                    <label>Keterangan</label>
                                    <textarea class="form-control" name="keterangan"></textarea>
                                </div>     
                            </div>
                            
                        </fieldset>
                        <br><br>
                        <div>
                            <button class="btn btn-primary" type="Submit">Simpan</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-primary fade bs-modal-lg-primary" tabindex="-1" barang="dialog" id="modaledit" aria-labelledby="myLargeModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-inverse">
                    <h5>Edit Data</h5>
                </div>
                <?php echo form_open_multipart($controller.'/submiteditdata'); ?>
                    <div class="modal-body">                    
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label>Kasus</label>
                                        <input class="form-control" type="text" required name="name" id="name">
                                    </div>              
                                    <div class="col-md-12">
                                        <label>Keterangan</label>
                                        <textarea class="form-control" name="keterangan" id="keterangan"></textarea>
                                    </div>     
                                </div>
                                
                                <input type="hidden" name="id" id="id">
                            </fieldset>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" type="Submit">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <?php include 'footer.php'; ?>

    <script type="text/javascript">
        function editdata(id) {
           $.ajax({
            url: "<?php echo site_url($controller.'/edit'); ?>/"+id,
            type:'GET',
            dataType: 'json',
            success: function(data){
              $("#id").val(data['list_edit']['id']);
              $("#name").val(data['list_edit']['name']);
              $("#keterangan").val(data['list_edit']['keterangan']);
                            
              $("#modaledit").modal('show');
            }, 
            error: function(){}
          }); 
        }

        function deletedata(id){
            var url="<?php echo site_url();?>";
            var r=confirm("Apakah Data ini ingin di Hapus?")
            if (r==true)
              window.location = url+"/<?php echo $controller; ?>/delete/"+id;
            else
              return false;
        }
    </script>
</body>
</html>
