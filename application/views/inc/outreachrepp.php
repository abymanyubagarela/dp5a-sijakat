<!doctype html>
<?php include 'header.php'; ?>

<style type="text/css">
    p {
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 1px;
        line-height: 1.42857143;
        color: #333;
    }
    @media print {
      .page-breaker {page-break-after: always;}
    }
</style>
<body>

    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->
        <div class="container">
            <div class="content pb-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="login-logo">
                            <a href="#">
                                <img class="align-content" src="<?= base_url()."uploads/kop.jpg"?>" alt="">
                            </a>
                        </div>
                    </div>  
                    <div class="col-lg-6 margin">
                        <div class="opening-out">
                            <table>
                                <tr>
                                    <td>Nomor</td>
                                    <td>:</td>
                                    <td>&nbsp; <?= $pre['no'] ?></td>
                                </tr>
                                <tr>
                                    <td>Sifat</td>
                                    <td>:</td>
                                    <td>&nbsp; <?= $pre['sifat'] ?></td>
                                </tr>
                                <tr>
                                    <td>Lampiran</td>
                                    <td>:</td>
                                    <td>&nbsp; <?= $pre['jumlah'] ?></td>
                                </tr>
                                <tr>
                                    <td>Hal</td>
                                    <td>:</td>
                                    <td>&nbsp; <?= $pre['hal'] ?></td>
                                </tr>
                            </table>    
                        </div>                    
                    </div>

                    <div class="col-lg-12 margin">
                        <div class="header-out">
                            Surabaya, <?= date('d M Y') ?> <br>

                            Kepada <br>
                            Yth. Walikota Surabaya <br>
                            di - <br>
                            <div> &nbsp; &nbsp; &nbsp;Surabaya</div>
                        </div>
                    </div>

                    <div class="col-lg-12 margin">
                        <div class="body-out">
                            <div class="event-body-out justify"> &nbsp; &nbsp; &nbsp; <?= $pre['pembuka'] ?></div>

                            <div class="client-body-out content-out">
                                <table>
                                    <tr>
                                        <td>Nama</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->name ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tempat/ Tgl Lahir</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->tempat.' / '. date('d M Y', strtotime($detail->tgl_lahir))?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Rumah</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->alamat ?></td>
                                    </tr>
                                    <tr>
                                        <td>No Telepon</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->telepon ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Wali</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->name_o ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->id_jeniskerja ?></td>
                                    </tr>
                                    <tr>
                                        <td>No Telepon Wali</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->telepon_o ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Saudara</td>
                                        <td class="dot">:</td>
                                        <td><?= $detail->jml_saudara ?></td>
                                    </tr>
                                </table>   
                            </div>

                            <div class="situasi-body-out content-out">
                                <div class="title"> Situasi Keluarga Klien</div>
                                <div class="content-situasi-body-out justify">
                                    &nbsp; &nbsp; &nbsp;<?= $detail->situasi ?>
                                   
                                </div>
                            </div>

                            <div class="kronologi-body-out content-out">
                                <div class="title"> Kronologi Kasus</div>
                                <div class="content-kronologi-body-out justify">
                                    &nbsp; &nbsp; &nbsp;<?= $detail->kronologi ?>
                                </div>
                            </div>

                            <div class="kesimpulan-body-out left">
                                <div class="content-kronologi-body-out justify">
                                    <?= $detail->kesimpulan ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 margin">
                        <div class="footer-out left-out">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="title"> Tembusan</div>
                                    <span>YTH.</span>
                                    <div class="tembusan">
                                        <?= $pre['tembusan'] ?>
                                    </div>
                                </div>
                                <div class="col-md-6 center-out">
                                    <div class="title"> KEPALA BADAN</div>
                                    <div class="name title"><?= $pre['ttd'] ?></div>
                                    <div><?= $pre['jabatan'] ?></div>
                                    <div>NIP. <?= $pre['nip'] ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-breaker"></div>
                    <div class="col-lg-12 margin photo">
                        <div class="situasi-body-out content-out">
                            <div class="row">
                                <div class="col-md-6">
                                    <img src='<?php echo !empty($detail->photo_1) ? base_url()."uploads/kronologi/".$detail->photo_1 : '' ;?>' style="max-width: 80%;" />
                                    <div class="title"> Foto 1</div>
                                </div>
                                <div class="col-md-6">
                                    <img src='<?php echo !empty($detail->photo_2) ? base_url()."uploads/kronologi/".$detail->photo_2 : '' ;?>' style="max-width: 80%;" />
                                    <div class="title"> Foto 2</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button id="print" class="btn btn-lg btn-success btn-block no-print">Print</button>
                </div>
                
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="clearfix"></div>

        <footer class="site-footer no-print">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2018 Ela Admin
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                </div>
            </div>
        </footer>
    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $( "#print" ).click(function() {
              window.print();
            });
        });
    </script>
</body>
</html>
