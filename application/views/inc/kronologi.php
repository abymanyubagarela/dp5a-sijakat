<!doctype html>
<?php include 'header.php'; ?>

<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                                <!-- filter -->
                <?php if ($this->session->userdata['auth']->id_role == 1) { ?>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="panel-options">
                                <h5>Filter</h5>
                            </div>
                        </div>
                        <div class="card-body card-block">
                            <?php echo form_open_multipart($controller); ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="form-control-label">Tanggal Awal</label>
                                            <input type="date" class="form-control" name="tgl_awal" required value=<?= isset($filter) ? date('Y-m-d', strtotime($filter['tgl_awal'])) : '' ?>>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-control-label">Tanggal Akhir</label>
                                            <input type="date" class="form-control" name="tgl_akhir" required value=<?= isset($filter) ? date('Y-m-d', strtotime($filter['tgl_akhir'])) : '' ?>>
                                        </div>

                                        <div class="col-md-3">
                                            <label class="form-control-label">Divisi</label>
                                            <select class="form-control" name="divisi">
                                                <?php foreach ($divisi as $key => $a) { ?>
                                                    <option value="<?php echo $a->id ?>" <?= isset($filter) ? ($filter['divisi'] == $key ? 'selected' : '')  : '' ?> ><?php echo $a->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>



                                        <div class="col-md-3">
                                            <label class="form-control-label">&nbsp;</label><br>
                                            <button type="submit" class="btn btn-sm btn-warning fa fa-search"></button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!-- filter -->
                
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="panel-options">
                                <!-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalcreate"><i class="fa fa-plus"></i>&nbsp; Tambah <?php echo $title; ?></button> -->
                                <h5>Data Tabel Kronologi</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="mastertable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Foto</th>
                                        
                                        <?php if ($this->session->userdata['auth']->id_role != 3) { ?>
                                        <th>Inputed</th>
                                        <?php } ?>
                                        
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0; ?>
                                    <?php foreach ($list as $key =>$row) { ?>
                                        <tr>                                                
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo $row->nik; ?></td>
                                            <td><?php echo $row->name; ?></td>
                                            <td>
                                                <?php if (!empty($row->photo_1)) { ?>
                                                    <img class="align-content" src="<?= base_url()."uploads/Kronologi/".$row->photo_1?>" alt="" width=300>
                                                <?php } ?>                                                
                                            </td>
                                            
                                            <?php if ($this->session->userdata['auth']->id_role != 3) { ?>
                                                <td><?php echo !empty($row->created_name) ? ucwords($row->created_name) : ''; ?></td>
                                            <?php } ?>

                                            <td width="15%" align="center">
                                                <?php if ($row->is_appoved) { ?>                                                    
                                                    <?php if ($this->session->userdata['auth']->id_role == 3) { ?>
                                                        <button type="button" class="btn btn-sm btn-success">Approved</button>
                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-sm btn-success fa fa-print" onclick="printdata(<?php echo $row->id;?>);"></button>
                                                    <?php } ?>             
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-sm btn-info fa fa-pencil" onclick="editdata(<?php echo $row->id;?>);"></button>
                                                    <button type="button" class="btn btn-sm btn-danger fa fa-eraser" onclick="deletedata(<?php echo $row->id;?>);"></button>

                                                    <?php if ($this->session->userdata['auth']->id_role == 1 || $this->session->userdata['auth']->id_role == 2) { ?>
                                                    <button type="button" class="btn btn-sm btn-success fa fa-check" onclick="editdata(<?php echo $row->id;?>);"></button>
                                                    <?php } ?>
                                                <?php } ?>             
                                            </td>                     
                                        </tr>
                                    <?php } ?>                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->
    <?php include 'footer.php'; ?>

    <script type="text/javascript">
        function editdata(id) {
            url = "<?php echo site_url($controller.'/editdata'); ?>/"+id,
            $(location).attr('href', url);
        }

        function deletedata(id){
            var url="<?php echo site_url();?>";
            var r=confirm("Apakah Data ini ingin di Hapus?")
            if (r==true)
              window.location = url+"/<?php echo $controller; ?>/delete/"+id;
            else
              return false;
        }

        function printdata(id) {
            url = "<?php echo site_url($controller.'/preprint'); ?>/"+id,
            $(location).attr('href', url);
        }
    </script>

</body>
</html>
