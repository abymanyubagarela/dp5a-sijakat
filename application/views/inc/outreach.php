<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12" style="padding: 0 10%;">
                    <?php echo form_open_multipart($controller.'/printdata'); ?>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body card-block">
                                    
                                    <div class="form-group">
                                        <label class="form-control-label">Pelapor</label>
                                        <input type="text" class="form-control" value="<?= $pelapor ?>" readonly>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Nomor Surat</label>
                                                <input type="text" class="form-control" placeholder="_____/______/123.4.5/2019" name="no">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Sifat</label>
                                                <input type="text" class="form-control" placeholder="Rahasia" name="sifat">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Jumlah Lampiran</label>
                                                <input type="number" class="form-control" name="jumlah">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-control-label">Hal</label>
                                                <input type="text" class="form-control" name="hal">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Paragraf Awal</label>
                                        <textarea class="form-control" name="pembuka"></textarea>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Penanda Tangan</label>
                                                <input type="text" class="form-control" name="ttd">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">Jabatan</label>
                                                <input type="text" class="form-control" placeholder="Kepala Pembina TK 1" name="jabatan">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="form-control-label">NIP</label>
                                                <input type="number" class="form-control" name="nip">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Tembusan</label>
                                        <textarea class="ckeditor form-control" name="tembusan"></textarea>
                                    </div>

                                    <input type="hidden" name="id_kronologi" value="<?= $id_kronologi ?>">
                                    <button type="submit" class="btn btn-lg btn-info">Cetak</button>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2018 Ela Admin
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Colorlib</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  
    </body>
</html>
