
<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open_multipart($controller.'/submiteditdata'); ?>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong><?php echo $title; ?></strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">Tanggal</label>
                                        <input type="date" class="form-control" name="tanggal" value="<?= date("Y-m-d", strtotime($edit->tanggal)) ?>" <?= $this->session->userdata['auth']->id_role != '3'? 'disabled':''; ?> >
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Klien</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">NIK</label>
                                        <input type="number" class="form-control" value="<?= !empty($edit->nik) ? $edit->nik : '' ?>" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" value="<?= !empty($edit->name) ? $edit->name : '' ?>" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Kategori</label>
                                        <input type="text" class="form-control" value="<?= !empty($edit->kategori) ? $edit->kategori : '' ?>" disabled>
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Kronologi</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">Situasi</label>
                                        <textarea rows="4" class="form-control" name="situasi"> <?= !empty($edit->situasi) ? $edit->situasi : '' ?> </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Kronologi</label>
                                        <textarea rows="4" class="form-control" name="Kronologi"> <?= !empty($edit->kronologi) ? $edit->kronologi : '' ?> </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Harapan</label>
                                        <textarea rows="4" class="form-control" name="harapan"> <?= !empty($edit->harapan) ? $edit->harapan : '' ?></textarea>
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Langkah</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">Langkah yang sudah di kerjakan</label>
                                        <textarea rows="4" class="form-control" name="pasca"><?= !empty($edit->pasca) ? $edit->pasca : '' ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Langkah yang akan di kerjakan</label>
                                        <textarea rows="4" class="form-control" name="pre"><?= !empty($edit->pre) ? $edit->pre : '' ?></textarea>
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Foto</button> -->
                                    <hr>


                                    <div class="form-group">
                                        <label class="form-control-label">Bantuan</label>
                                        <textarea rows="4" class="form-control" name="bantuan"> <?= !empty($edit->bantuan) ? $edit->bantuan : '' ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Monitoring</label>
                                        <textarea rows="4" class="form-control" name="monitoring"> <?= !empty($edit->monitoring) ? $edit->monitoring : '' ?></textarea>
                                    </div>
                                                                
                                    <div class="form-group">
                                        <label class="form-control-label">Foto 1</label>
                                                
                                        <input class="form-control inputfile_e" type="file" name="photo_1" <?= $this->session->userdata['auth']->id_role != '3'? 'disabled':''; ?>>
                                        <br>
                                        <img id="photo_1" src='<?php echo !empty($edit->photo_1) ? base_url()."uploads/kronologi/".$edit->photo_1 : '' ;?>' style="max-width: 50%;margin: auto 25%;" /> <br>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Foto 2</label>
                                                
                                        <input class="form-control inputfile_e" type="file" name="<?= 'photo_2' ?>" <?= $this->session->userdata['auth']->id_role != '3'? 'disabled':''; ?>>
                                        <br>
                                        <img id="photo_2" src='<?php echo !empty($edit->photo_2) ? base_url()."uploads/kronologi/".$edit->photo_2 : '' ;?>' style="max-width: 50%;margin: auto 25%;" /> <br>
                                    </div>
                                    
                                    
                                    <?php if ($this->session->userdata['auth']->id_role != '3') { ?>
                                        <div class="form-group">
                                            <label class="form-control-label">Kesimpulan</label>
                                            <textarea class="ckeditor form-control" name="kesimpulan" id="kesimpulan"><?= !empty($edit->kesimpulan) ? $edit->kesimpulan : '' ?></textarea>
                                        </div>
                                    <?php } ?>

                                    <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
                                    <?php if ($this->session->userdata['auth']->id_role != '3') { ?>
                                        <button type="submit" class="btn btn-lg btn-danger">Setujui</button>
                                    <?php } else { ?>
                                        <button type="submit" class="btn btn-lg btn-info">Simpan</button>
                                    <?php } ?>
                                </div>
                            </div>                            
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  

    <script type="text/javascript">
        $(".inputfile_e").change(function () {
            readURL(this);
        });

        function readURL(input) {
            // alert(input.name);
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#'+input.name).attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        <?php if ($this->session->userdata['auth']->id_role != '3') { ?>
        $("textarea").prop("disabled", true);
        $("#kesimpulan").prop("disabled", false);           
        <?php } ?>     
    </script>
</body>
</html>
