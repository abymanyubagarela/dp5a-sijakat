<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open_multipart($controller.'/submiteditdata'); ?>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong><?php echo $title; ?></strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">NIK</label>
                                        <input type="number" class="form-control" name="nik" value="<?= !empty($edit->nik) ? $edit->nik : '' ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" name="name" value="<?= !empty($edit->name) ? $edit->name : '' ?>">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tempat</label>
                                                <input type="text" class="form-control" name="tempat" value="<?= !empty($edit->tempat) ? $edit->tempat : '' ?>">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="tgl_lahir" value='<?= date("Y-m-d", strtotime($edit->tgl_lahir)); ?>'>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kelamin</label>
                                                 <select class="form-control" name="kelamin">
                                                    <option value="0" <?= $edit->kelamin == '0' ? 'selected' : '';?> >Laki - laki</option>
                                                    <option value="1" <?= $edit->kelamin == '1' ? 'selected' : '';?> >Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Telepon</label>
                                                <input type="text" class="form-control" name="telepon" value="<?= !empty($edit->telepon) ? $edit->telepon : '' ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Alamat</label>
                                        <textarea rows="2" class="form-control" name="alamat"> <?= !empty($edit->alamat) ? $edit->alamat : '' ?>  </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Alamat KK</label>
                                        <textarea rows="2" class="form-control" name="alamat_kk"> <?= !empty($edit->alamat_kk) ? $edit->alamat_kk : '' ?> </textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="form-control-label">No KK</label>
                                        <input type="number" class="form-control" name="no_kk" value="<?= !empty($edit->no_kk) ? $edit->no_kk : '' ?>">
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kecamatan</label>
                                                 <select class="form-control" name="id_kecamatan">
                                                    <?php foreach ($kecamatan as $key => $c) { ?>
                                                        <option value="<?php echo $c->id ?>" <?= $edit->id_kecamatan == $c->id ? 'selected' : '';?> ><?php echo $c->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Kelurahan</label>
                                                 <select class="form-control" name="id_kelurahan">
                                                    <?php foreach ($kelurahan as $key => $l) { ?>
                                                        <option value="<?php echo $l->id ?>" <?= $edit->id_kelurahan == $l->id ? 'selected' : '';?> ><?php echo $l->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="form-control-label">Agama</label>
                                                 <select class="form-control" name="id_agama">
                                                    <?php foreach ($agama as $key => $a) { ?>
                                                        <option value="<?php echo $a->id ?>" <?= $edit->id_agama == $a->id ? 'selected' : '';?> ><?php echo $a->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="form-control-label">Status Kawin</label>
                                                 <select class="form-control" name="status">
                                                    <option value="0" <?= $edit->status == '0' ? 'selected' : '';?>>Belum</option>
                                                    <option value="1" <?= $edit->status == '1' ? 'selected' : '';?>>Kawin</option>
                                                    <option value="2" <?= $edit->status == '2' ? 'selected' : '';?>>Bercerai</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Jenis Pekerjaan</label>
                                        <select class="form-control" name="id_jeniskerja">
                                            <?php foreach ($kerja as $key => $r) { ?>
                                                <option value="<?php echo $r->id ?>" <?= $edit->id_jeniskerja == $r->id ? 'selected' : '';?> ><?php echo $r->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pekerjaan</label>
                                        <input type="text" class="form-control" name="pekerjaan" value="<?= !empty($edit->pekerjaan) ? $edit->pekerjaan : '' ?>" >
                                    </div>

                                </div>
                            </div>
                        </div>
                   
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong>Data Wali</strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">Nama</label>
                                        <input type="text" class="form-control" name="name_o" value="<?= !empty($edit->name_o) ? $edit->name_o : '' ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Telepon</label>
                                        <input type="text" class="form-control" name="telepon_o" value="<?= !empty($edit->telepon_o) ? $edit->telepon_o : '' ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Jenis Pekerjaan</label>
                                        <select class="form-control" name="id_jeniskerja_o">
                                            <?php foreach ($kerja as $key => $r) { ?>
                                                <option value="<?php echo $r->id ?>" <?= $edit->id_jeniskerja_o == $r->id ? 'selected' : '';?> ><?php echo $r->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Nama Pekerjaan</label>
                                        <input type="text" class="form-control" name="pekerjaan_o" value="<?= !empty($edit->pekerjaan_o) ? $edit->pekerjaan_o : '' ?>">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Jml Saudara</label>
                                        <input type="text" class="form-control" name="jml_saudara" value="<?= !empty($edit->jml_saudara) ? $edit->jml_saudara : '' ?>">
                                    </div>

                                    <div class="form-group">
                                        <input type="hidden" name="id" value="<?= $edit->id;  ?>">
                                        <button type="submit" class="btn btn-lg btn-info btn-block">Simpan</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 DP5A
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://colorlib.com">Zahin Victor</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $("#datetimepickerFrom").datepicker({
                defaultDate: '<?php echo $newDate; ?>'
            });
        });
    </script>
</body>
</html>
