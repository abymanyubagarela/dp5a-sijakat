<header id="header" class="header no-print">  
    <div class="top-left">
        <div class="navbar-header"> 
            <a class="navbar-brand" href="<?php echo base_url().'welcome'; ?>"><img src="https://s3.amazonaws.com/owler-image/logo/outreach_owler_20180802_165453_original.png" alt="Logo"></a>
            
            <a class="navbar-brand hidden" href="<?php echo base_url().'welcome'; ?>"><img src="https://s3.amazonaws.com/owler-image/logo/outreach_owler_20180802_165453_original.png" alt="logo"></a> 
            
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a> 
        </div> 
    </div>
</header>