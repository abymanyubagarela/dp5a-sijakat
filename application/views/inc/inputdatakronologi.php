<!doctype html>
<?php include 'header.php'; ?>
<body>
    <!-- Left Panel --> 
    <?php include 'sidebar.php'; ?>
    <!-- Left Panel -->

    <!-- Right Panel --> 
    <div id="right-panel" class="right-panel" style="background-color: #fff">

        <!-- Header-->
        <?php include 'header-right.php'; ?>
        <!-- Header-->

        <div class="content pb-0">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open_multipart($controller.'/submitadddata'); ?>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header"><strong><?php echo $title; ?></strong></div>
                                <div class="card-body card-block">
                                    <div class="form-group">
                                        <label class="form-control-label">Tanggal</label>
                                        <input type="date" class="form-control" name="tanggal">
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Klien</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">NIK</label>
                                        <select class="form-control e1" name="id_klien" id="id_klien">
                                            <?php foreach ($klien as $key => $k) { ?>
                                                <option value="<?php echo $k->id ?>"><?php echo $k->nik.' - '.$k->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="form-control-label">Kategori Kasus</label>
                                                 <select class="form-control e1" name="id_kategori">
                                                    <?php foreach ($kategori as $key => $a) { ?>
                                                        <option value="<?php echo $a->id ?>"><?php echo $a->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Kronologi</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">Situasi</label>
                                        <textarea rows="4" class="form-control" name="situasi"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Kronologi</label>
                                        <textarea rows="4" class="form-control" name="Kronologi"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Harapan</label>
                                        <textarea rows="4" class="form-control" name="harapan"></textarea>
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Langkah</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">Langkah yang sudah di kerjakan</label>
                                        <textarea rows="4" class="form-control" name="pasca"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Langkah yang akan di kerjakan</label>
                                        <textarea rows="4" class="form-control" name="pre"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Intervensi</label>
                                                 <select class="form-control" name="status">
                                                    <option value="0">Psikologis</option>
                                                    <option value="1">Bantuan</option>
                                                    <option value="2">Hukum</option>
                                                </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Monitoring</label>
                                        <textarea rows="4" class="form-control" name="monitoring"></textarea>
                                    </div>

                                    <!-- <button type="button" class="btn btn-info m-l-1 m-b-10">Foto</button> -->
                                    <hr>
                                    <div class="form-group">
                                        <label class="form-control-label">Foto 1</label>
                                        <input type="file" class="form-control" name="photo_1">
                                    </div>

                                    <div class="form-group">
                                        <label class="form-control-label">Foto 2</label>
                                        <input type="file" class="form-control" name="photo_2">
                                    </div>

                                    <div class="row" style="padding: 0 30%">
                                        <div class="col-md-6"><button type="submit" class="btn btn-lg btn-info btn-block">Simpan</button></div>
                                        <div class="col-md-6">
                                            <a href="<?php echo base_url().'kronologi/inputdata'; ?>" class="btn btn-lg btn-danger btn-block">Batal</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="clearfix"></div>
            
        </div> <!-- .content -->



        <div class="clearfix"></div>

        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2020 DP5A Surabaya
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="https://dp5a.surabaya.go.id">DP5A Surabaya</a>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- /#right-panel -->

    <?php include 'footer.php'; ?>  

    <script type="text/javascript">
        // $( "#id_klien" ).change(function() {
        //   alert( $("#id_klien").text());
        // });
    </script>
</body>
</html>
